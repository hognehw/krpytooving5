cmake_minimum_required(VERSION 3.1)

project(Oving5)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11 -Wall -Wextra")

add_library(utility INTERFACE)
target_include_directories(utility INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

add_executable(C1 C1.c)
target_link_libraries(C1 utility)

enable_testing()
add_subdirectory(tests)
